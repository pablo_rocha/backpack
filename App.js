import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/es/integration/react'

import configureStore from './src/store'
import AppWithNavigationState from './src/navigator'
import Loading from './src/components/loading'

const { persistor, store } = configureStore()

const onBeforeLift = () => {
  // take some action before the gate lifts
}

export default class Backpack extends Component {
  store = store

  constructor (props) {
    super(props)
  }

  render() {
    return (
      <Provider store={this.store}>
        <PersistGate loading={<Loading />} onBeforeLift={onBeforeLift} persistor={persistor}>
          <AppWithNavigationState />
        </PersistGate>
      </Provider>
    )
  }
}