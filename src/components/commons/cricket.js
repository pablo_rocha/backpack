import { DeviceEventEmitter, Text, TouchableOpacity } from 'react-native'
import React, { Component } from 'react'

export default class CricketComponent extends Component {
  componentDidMount () {
    console.log(['Backpack', 'Cricket.componentDidMount'])
    DeviceEventEmitter.addListener('cricket', this.listener)
  }

  componentWillUnmount () {
    console.log(['Backpack', 'Cricket.componentWillUnmount'])
    DeviceEventEmitter.removeListener('cricket', this.listener)
  }

  listener = (data) => {
    console.log(['Backpack', 'Cricket.listener', data])
  }

  render () {
    return null
  }
}