import { Text, TouchableOpacity } from 'react-native'
import React, { Component } from 'react'

export default class ButtonComponent extends Component {
  onPress = () => {
    if (this.props.onPress) {
      return this.props.onPress()
    }
    this.props.navigate(this.props.action)
  }

  render () {
    return (
      <TouchableOpacity onPress={this.onPress} style={{ padding: 5, margin: 5, backgroundColor: '#1abc9c' }}>
        <Text style={{ textAlign: 'center' }}>{this.props.label.toUpperCase()}</Text>
      </TouchableOpacity>
    )
  }
}