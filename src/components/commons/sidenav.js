import { Text, View } from 'react-native'
import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import CricketComponent from './cricket'
import ButtonComponent from './button'

export default class SidenavComponent extends Component {
  componentDidMount () {
    console.log(['Backpack', 'Sidenav.componentDidMount'])
  }

  componentWillUnmount () {
    console.log(['Backpack', 'Sidenav.componentWillUnmount'])
  }

  logout = () => {
    // const { navigate } = this.props.navigation
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'LoginNav' })
      ],
      key: null
    });
    this.props.navigation.dispatch(resetAction);
  }

  register = () => {
    // this.props.navigate('Reset', { action: 'Register' })
    // const { navigate } = this.props.navigation
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'ResetNav', params: { action: 'Register' } })
      ],
      key: null
    });
    this.props.navigation.dispatch(resetAction);
  }

  render () {
    return (
      <View>
          <Text>Sidenav Screen</Text>
          <ButtonComponent label={'Dashboard'} action={'Dashboard'} navigate={this.props.navigation.navigate} />
          <ButtonComponent label={'Register'} onPress={this.register} navigate={this.props.navigation.navigate} />
          <ButtonComponent label={'Help'} action={'Help'} navigate={this.props.navigation.navigate} />
          <ButtonComponent label={'Logout'} onPress={this.logout} navigate={this.props.navigation.navigate} />
          <CricketComponent />
      </View>
    )
  }
}