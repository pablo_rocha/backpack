import { DeviceEventEmitter, Text, View } from 'react-native'
import React, { Component } from 'react'

export default class BaseComponent extends Component {
  componentDidMount () {
    console.log(['Backpack', 'Base.componentDidMount'])
    DeviceEventEmitter.addListener('message', this.messageListener)
  }

  componentWillUnmount () {
    console.log(['Backpack', 'Base.componentWillUnmount'])
    DeviceEventEmitter.removeListener('message', this.messageListener)
  }

  messageListener = (data) => {
    console.log(['Backpack', 'Base.messageListener', data])
  }

  render () {
    return (
      <View>{this.props.children}</View>
    )
  }
}