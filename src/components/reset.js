import { Text, View } from 'react-native'
import React, { Component } from 'react'
import { connect } from 'react-redux'

class ResetComponent extends Component {
  navigation = {}
  params = []

  constructor (props) {
    super(props)
    this.navigation = props.navigation
    this.params = this.navigation.state.params
  }

  componentDidMount () {
    console.log(['Backpack', 'Reset.componentDidMount', this.params])
    this.navigation.navigate(this.params.action)
  }

  componentWillUnmount () {
    console.log(['Backpack', 'Reset.componentWillUnmount'])
  }

  render () {
    return null
  }
}

const mapStateToProps = (state) => {
  return ({
    state: state
  })
}

export default connect(mapStateToProps)(ResetComponent)
