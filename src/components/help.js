import { Text, View } from 'react-native'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import events from '../services/events'
import BaseComponent from './commons/base'
import ButtonComponent from './commons/button'

class HelpComponent extends Component {
  componentDidMount () {
    console.log(['Backpack', 'Help.componentDidMount'])
  }

  componentWillUnmount () {
    console.log(['Backpack', 'Help.componentWillUnmount'])
  }

  emitMessage = () => {
    events.emit('message', { name: 'Help!' })
    events.emit('cricket', { name: 'Help!' })
  }

  render () {
    return (
      <BaseComponent>
          <Text>Help Screen</Text>
          <Text>>>>> open sidenav >>>></Text>
          <ButtonComponent label={'Emit Message'} onPress={this.emitMessage} navigate={this.props.navigation.navigate} />
      </BaseComponent>
    )
  }
}

const mapStateToProps = (state) => {
  return ({
    state: state
  })
}

export default connect(mapStateToProps)(HelpComponent)
