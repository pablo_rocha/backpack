import { Text, View, TextInput } from 'react-native'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ACTUser } from '../reducers/user'

import ButtonComponent from './commons/button'

class LoginComponent extends Component {
  componentDidMount () {
    console.log(['Backpack', 'Login.componentDidMount'])
  }

  componentWillUnmount () {
    console.log(['Backpack', 'Login.componentWillUnmount'])
  }

  render () {
    return (
      <View>
          <Text>Login Screen</Text>

          <TextInput
            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
            onChangeText={(value) => this.props.update({ email: value })}
            value={this.props.state.email}
          />
          
          <ButtonComponent label={'Login'} action={'Dashboard'} navigate={this.props.navigation.navigate} />
          <ButtonComponent label={'Register'} action={'Register'} navigate={this.props.navigation.navigate} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return ({
    state: state.user
  })
}

const mapDispatchToProps = (dispatch) => ({
  update: (data) => dispatch(ACTUser.update(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent)
