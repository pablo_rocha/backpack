import { Text, View } from 'react-native'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import ButtonComponent from './commons/button'

class RegisterComponent extends Component {
  componentDidMount () {
    console.log(['Backpack', 'Register.componentDidMount'])
  }

  componentWillUnmount () {
    console.log(['Backpack', 'Register.componentWillUnmount'])
  }

  render () {
    return (
      <View>
          <Text>Register Screen</Text>
          <ButtonComponent label={'Submit'} action={'Dashboard'} navigate={this.props.navigation.navigate} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return ({
    state: state
  })
}

export default connect(mapStateToProps)(RegisterComponent)
