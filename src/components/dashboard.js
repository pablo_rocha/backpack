import { Text, View } from 'react-native'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import events from '../services/events'
import BaseComponent from './commons/base'
import ButtonComponent from './commons/button'

class DashboardComponent extends Component {
  componentDidMount () {
    console.log(['Backpack', 'Dashboard', 'props.state', this.props.state])    
    console.log(['Backpack', 'Dashboard.componentDidMount'])    
  }

  componentWillUnmount () {
    console.log(['Backpack', 'Dashboard.componentWillUnmount'])    
  }  

  emitMessage = () => {
    events.emit('message', { name: 'Dashboard!' })
    events.emit('cricket', { name: 'Dashboard!' })
  }

  render () {
    return (
      <BaseComponent>
          <Text>Dashboard Screen</Text>
          <Text>>>>> open sidenav >>>></Text>
          <Text>email: {this.props.state.email}</Text>
          <ButtonComponent label={'Emit Message'} onPress={this.emitMessage} navigate={this.props.navigation.navigate} />
      </BaseComponent>
    )
  }
}

const mapStateToProps = (state) => {
  return ({
    state: state.user
  })
}

export default connect(mapStateToProps)(DashboardComponent)
