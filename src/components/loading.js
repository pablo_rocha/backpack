import React, { Component } from 'react'
import { DeviceEventEmitter, StyleSheet, View, Text } from 'react-native'
import PropTypes from 'prop-types'

export default class Loading extends Component {
  render () {
    return (
      <View style={styles.main}>
        <View style={styles.container}>
          <Text>Loading...</Text>
        </View>  
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1
  },
  container: {
    flex: 1,
    alignSelf: 'center',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
