import { applyMiddleware, compose, createStore } from 'redux'
import { persistStore, persistCombineReducers } from 'redux-persist'
import thunk from 'redux-thunk'
import devTools from 'remote-redux-devtools'
import storage from 'redux-persist/es/storage'
import { reducers } from './reducers'



const enhancer = compose(
  applyMiddleware(
    thunk
  ),
  devTools()
)

const config = {
  key: 'root',
  blacklist: ['nav', 'navigation'],
  storage
}

const reducer = persistCombineReducers(config, reducers)

const configureStore = () => {
  // ...
  let store = createStore(reducer, enhancer)
  let persistor = persistStore(store)

  return { persistor, store }
}

export default configureStore
