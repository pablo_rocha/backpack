export const type = {
  CREATE: 'user/CREATE',
  UPDATE: 'user/UPDATE',
  DESTROY: 'user/DESTROY'
}

export class ACTUser {
  static create (data) {
    return ({
      type: type.CREATE,
      data
    })
  }

  static update (data) {
    return ({
      type: type.UPDATE,
      data
    })
  }

  static destroy () {
    return ({
      type: type.DESTROY
    })
  }
}

export const initialState = {
  data: {
    email: 'undefined@mail.com'
  }
}

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case type.CREATE:
      return { ...action.data }
    case type.UPDATE:
      return { ...action.data }
    case type.DESTROY:
      return initialState
    default:
      return state
  }
}
