import { addNavigationHelpers, DrawerNavigator, StackNavigator, NavigationActions } from 'react-navigation'
import React, { Component } from 'react'
import { BackHandler, Platform } from 'react-native'
import { connect } from 'react-redux'

// import { BugFreeStackNavigator } from './services/bug-free-stack-navigator'

import LoginComponent from './components/login'
import RegisterComponent from './components/register'
import DashboardComponent from './components/dashboard'
import ResetComponent from './components/reset'
import HelpComponent from './components/help'
import SidenavComponent from './components/commons/sidenav'

const settings = {
  headerMode: 'none',
  mode: Platform.OS === 'ios' ? 'modal' : 'card'
}

const stack = StackNavigator(
  {
    Login: { screen: LoginComponent },
    Register: { screen: RegisterComponent }
  },
  {
    ...settings,
    initialRouteName: 'Login'
  }
)

const reset = StackNavigator(
  {
    Reset: { screen: ResetComponent }
  },
  {
    ...settings,
    initialRouteName: 'Reset'
  }
)

const drawer = DrawerNavigator(
  {
    Dashboard: { screen: DashboardComponent },
    Help: { screen: HelpComponent }
  },
  {
    ...settings,
    drawerPosition: Platform.OS === 'ios' ? 'right' : 'left',
    contentComponent: (props) => <SidenavComponent {...props} />
  }
)

export const AppNavigator = StackNavigator(
  {
    LoginNav: { screen: stack },
    DrawerNav: { screen: drawer },
    ResetNav: { screen: reset }
  },
  {
    ...settings,
    initialRouteName: 'LoginNav'
  }
)

class AppWithNavigationState extends Component {
  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
  }

  onBackPress = () => {
    const { dispatch, nav } = this.props
    const hasChild = nav.routes[0].index !== 0

    // minimize the app when login screen
    if (nav.routes.length === 1 && !hasChild) {
      return false
    }

    dispatch(NavigationActions.back())
    return true
  }

  render() {
    const { dispatch, nav } = this.props
    return (
      <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
    )
  }
}

const mapStateToProps = state => ({
  nav: state.nav
})

export default connect(mapStateToProps)(AppWithNavigationState)
