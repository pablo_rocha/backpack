import { combineReducers } from 'redux'
import { NavigationActions } from 'react-navigation'
import { AppNavigator } from './navigator'
import { reducer as user } from './reducers/user'

const initialNavState = AppNavigator.router.getStateForAction(
  NavigationActions.init()
)

function nav(state = initialNavState, action) {
  let nextState;
  // console.log(['Backpack', 'reducers.nav', action])

  /* if (action.type === 'Navigation/RESET' && action.actions && action.actions[0] && action.actions[0].action) {
    nextState = AppNavigator.router.getStateForAction(
      NavigationActions.navigate({ routeName: action.actions[0].action }),
      state
    );
    return nextState || state;
  } */

  switch (action.routeName) {
    /* case 'Login':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.back(),
        state
      );
      break;
    case 'Logout':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Login' }),
        state
      );
      break; */
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}

/* export const reducers = combineReducers({
  nav: nav
}) */

export const reducers = {
  nav: nav,
  user: user
}